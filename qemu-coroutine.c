/*
 * QEMU coroutines
 *
 * Copyright IBM, Corp. 2011
 *
 * Authors:
 *  Stefan Hajnoczi    <stefanha@linux.vnet.ibm.com>
 *  Kevin Wolf         <kwolf@redhat.com>
 *
 * This work is licensed under the terms of the GNU LGPL, version 2 or later.
 * See the COPYING.LIB file in the top-level directory.
 *
 */

/* XXX Is there a nicer way to disable glibc's stack check for longjmp? */
#ifdef _FORTIFY_SOURCE
#undef _FORTIFY_SOURCE
#endif
#include <setjmp.h>

#include "trace.h"
#include "qemu-queue.h"
#include "qemu-common.h"
#include "qemu-coroutine.h"
#include "qemu-coroutine-int.h"

enum {
    /* Maximum free pool size prevents holding too many freed coroutines */
    POOL_MAX_SIZE = 64,
};

static QLIST_HEAD(, Coroutine) pool = QLIST_HEAD_INITIALIZER(&pool);
static unsigned int pool_size;
static __thread Coroutine leader;
static __thread Coroutine *current;

static void coroutine_delete(Coroutine *co)
{
    qemu_free(co->stack);
    qemu_free(co);
}

static void coroutine_terminate(Coroutine *co)
{
    trace_qemu_coroutine_terminate(co);

    if (pool_size < POOL_MAX_SIZE) {
        QLIST_INSERT_HEAD(&pool, co, pool_next);
        co->caller = NULL;
        pool_size++;
    } else {
        coroutine_delete(co);
    }
}

static Coroutine *coroutine_new(void)
{
    const size_t stack_size = 4 << 20;
    Coroutine *co;

    co = qemu_mallocz(sizeof(*co));
    co->stack = qemu_malloc(stack_size);
    qemu_coroutine_init_env(co, stack_size);
    return co;
}

Coroutine *qemu_coroutine_create(CoroutineEntry *entry)
{
    Coroutine *co;

    co = QLIST_FIRST(&pool);

    if (co) {
        QLIST_REMOVE(co, pool_next);
        pool_size--;
    } else {
        co = coroutine_new();
    }

    co->entry = entry;

    return co;
}

Coroutine *coroutine_fn qemu_coroutine_self(void)
{
    if (current == NULL) {
        current = &leader;
    }

    return current;
}

bool qemu_in_coroutine(void)
{
    return qemu_coroutine_self() != &leader;
}

static void *coroutine_swap(Coroutine *from, Coroutine *to, void *opaque)
{
    int ret;
    void *to_data;

    to->data = opaque;

    ret = setjmp(from->env);
    switch (ret) {
    case COROUTINE_YIELD:
        return from->data;
    case COROUTINE_TERMINATE:
        current = to->caller;
        to_data = to->data;
        coroutine_terminate(to);
        return to_data;
    default:
        /* Switch to called coroutine */
        current = to;
        longjmp(to->env, COROUTINE_YIELD);
        return NULL;
    }
}

void qemu_coroutine_enter(Coroutine *co, void *opaque)
{
    Coroutine *self = qemu_coroutine_self();

    trace_qemu_coroutine_enter(self, co, opaque);

    if (co->caller) {
        fprintf(stderr, "Co-routine re-entered recursively\n");
        abort();
    }

    co->caller = self;
    coroutine_swap(self, co, opaque);
}

void *coroutine_fn qemu_coroutine_yield(void)
{
    Coroutine *self = qemu_coroutine_self();
    Coroutine *to = self->caller;

    trace_qemu_coroutine_yield(self, self->caller);

    if (!to) {
        fprintf(stderr, "Co-routine is yielding to no one\n");
        abort();
    }

    self->caller = NULL;
    return coroutine_swap(self, to, NULL);
}

static void coroutine_free_pool(void)
{
    Coroutine *co;
    Coroutine *tmp;

    QLIST_FOREACH_SAFE(co, &pool, pool_next, tmp) {
        coroutine_delete(co);
    }
    pool_size = 0;
}

void qemu_coroutine_init(void)
{
    atexit(coroutine_free_pool);
}
